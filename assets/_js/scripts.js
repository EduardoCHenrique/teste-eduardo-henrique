
$(function() {
  $('#menu-icon').click(function(){
    $('.web-app').find('.container').toggleClass('menu-active');
  });

  $('#profiles-icon').click(function(){
  	$('header').find('.profiles').toggleClass('active');
  });

  $('#chat-icon').click(function(){
  	$('article').find('.notification-box').toggleClass('active');
  });

  $('.logo').click(function(){
  	$('*').removeClass('active');
  });

$('#add-profile').click(function(){
  	$('article').find('#map_canvas').toggleClass('blur');
  	$('article').find('#add-form').toggleClass('active');

  });


});